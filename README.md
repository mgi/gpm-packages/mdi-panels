The MDI Panels help a LabVIEW user quickly and easily implement a Multiple Document Interface (MDI) in LabVIEW. This require two components

## MDI Container

The MDI Container is where the MDI windows live. This is implemented using a .NET Container with a System.Windows.Forms.Panel Type.

## MDI Panel

This is the actual Panel that is the MDI Child window. Give this Panel type to your UI implementation to put it in the MDI Panel. The MDI Panel is very similar to the Window Panel type, so it uses some of the same functionality (such as window sizing and alignment)

## Contributing

See [Contributing.md](CONTRIBUTING.md) for information on how to submit pull requests. Bugs can be reported using the repositories issue tracker.

#### _This package is implemented with LabVIEW 2017_
